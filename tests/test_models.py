import keras_resnet.models
from tensorflow import keras
from keras.utils.layer_utils import count_params


class TestResNet18:
    def test_constructor(self, x):
        model = keras_resnet.models.ResNet2D18(x)

        assert len(model.layers) == 87


class TestResNet34:
    def test_constructor(self, x):
        model = keras_resnet.models.ResNet2D34(x)

        assert len(model.layers) == 159


class TestResNet50:
    def test_constructor(self, x):
        model = keras_resnet.models.ResNet2D50(x)

        assert len(model.layers) == 191


class TestResNet101:
    def test_constructor(self, x):
        model = keras_resnet.models.ResNet2D101(x)

        assert len(model.layers) == 378


class TestResNet152:
    def test_constructor(self, x):
        model = keras_resnet.models.ResNet2D152(x)

        assert len(model.layers) == 565


class TestResNet200:
    def test_constructor(self, x):
        model = keras_resnet.models.ResNet2D200(x)

        assert len(model.layers) == 741


class TestResNet1D18:
    def test_constructor(self, x_1d):
        model = keras_resnet.models.ResNet1D18(x_1d)

        assert len(model.layers) == 71


class TestResNet1D34:
    def test_constructor(self, x_1d):
        model = keras_resnet.models.ResNet1D34(x_1d)

        assert len(model.layers) == 127


class TestResNet1D50:
    def test_constructor(self, x_1d):
        model = keras_resnet.models.ResNet1D50(x_1d)

        assert len(model.layers) == 175


class TestResNet1D101:
    def test_constructor(self, x_1d):
        model = keras_resnet.models.ResNet1D101(x_1d)

        assert len(model.layers) == 345


class TestResNet1D152:
    def test_constructor(self, x_1d):
        model = keras_resnet.models.ResNet1D152(x_1d)

        assert len(model.layers) == 515


class TestResNet1D200:
    def test_constructor(self, x_1d):
        model = keras_resnet.models.ResNet1D200(x_1d)

        assert len(model.layers) == 675


class TestResNet1D18_shape:
    def test_constructor(self):
        model = keras_resnet.models.ResNet1D18(keras.layers.Input((2560, 8)))

        assert len(model.layers) == 71


class TestResNet1D18_conv_fun:
    def test_constructor(self, x_1d):
        model = keras_resnet.models.ResNet1D18(
            x_1d, conv_fun=keras.layers.SeparableConv1D
        )

        assert len(model.layers) == 71
        assert count_params(model.trainable_weights) <= 2e6


class TestResNet1D18_n_fitlers:
    def test_constructor(self, x_1d):
        model = keras_resnet.models.ResNet1D18(x_1d, n_filters=16)

        assert len(model.layers) == 71
        assert model.layers[-1].input.shape[-1] == 16 * 2 * 2 * 2


class TestResNet1D18_kernel_size:
    def test_constructor(self, x_1d):
        model = keras_resnet.models.ResNet1D18(x_1d, kernel_size=15)

        assert len(model.layers) == 71
        assert model.layers[-6].kernel_size == (15,)


class TestResNet1D18_top_activiation:
    def test_constructor(self, x_1d):
        model = keras_resnet.models.ResNet1D18(x_1d, top_activiation="sigmoid")

        assert len(model.layers) == 71
        assert model.layers[-1].activation.__name__ == "sigmoid"


class TestResNet1D34_shape:
    def test_constructor(self):
        model = keras_resnet.models.ResNet1D34(keras.layers.Input((2560, 8)))

        assert len(model.layers) == 127


class TestResNet1D34_conv_fun:
    def test_constructor(self, x_1d):
        model = keras_resnet.models.ResNet1D34(
            x_1d, conv_fun=keras.layers.SeparableConv1D
        )

        assert len(model.layers) == 127
        assert count_params(model.trainable_weights) <= 3.2e6


class TestResNet1D34_n_fitlers:
    def test_constructor(self, x_1d):
        model = keras_resnet.models.ResNet1D34(x_1d, n_filters=16)

        assert len(model.layers) == 127
        assert model.layers[-1].input.shape[-1] == 16 * 2 * 2 * 2


class TestResNet1D34_kernel_size:
    def test_constructor(self, x_1d):
        model = keras_resnet.models.ResNet1D34(x_1d, kernel_size=15)

        assert len(model.layers) == 127
        assert model.layers[-6].kernel_size == (15,)


class TestResNet1D34_top_activiation:
    def test_constructor(self, x_1d):
        model = keras_resnet.models.ResNet1D34(x_1d, top_activiation="sigmoid")

        assert len(model.layers) == 127
        assert model.layers[-1].activation.__name__ == "sigmoid"
